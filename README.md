# Overview #

Cron-like service for scheduled jobs

1. Possible to create jobs dynamically by other services
2. Supports cron-like syntax
3. Supports calling REST API-s, all methods


# Create a new job

Possible values:

* second 0-59 (OPTIONAL, default = 0)
* minute 0-59
* hour 0-23
* dayOfMonth 1-31
* month 1-12
* dayOfWeek 0-7 (0 or 7 is Sunday)

```
curl -H 'Content-type: application/json' -X POST http://localhost:8080/v1/jobs
{
    "owner": "booking-mail-sender",
    "name": "check-scheduled-mail",
    "rule": "* * * * * * *",
    "callback": {
        "url": "http://dev3.localnet:8001/status",
        "method:" GET"
    }
}
```

# Get registered jobs

```
curl -X GET http://localhost:8080/v1/jobs
```

# Get one job

```
curl -X GET http://localhost:8080/v1/jobs/591aa7eb7dc62c2b4e9ae312
```

# Delete a job

```
curl -X DELETE http://localhost:8080/v1/jobs/591aa7eb7dc62c2b4e9ae312
```

# Activate or deactivate job

```
curl -X PATCH http://localhost:8080/v1/jobs/591aa7eb7dc62c2b4e9ae312
{
    "active": false
}
```

# Execute a job immediately

```
curl -X POST http://localhost:8080/v1/jobs/591aa7eb7dc62c2b4e9ae312/execute
```