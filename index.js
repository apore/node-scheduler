'use strict';
var logger = require('winston');

try {
	logger.configure({
		transports : [
			new logger.transports.Console({
				handleExceptions: false,
				json: parseInt(process.env.LOG_JSON),
				stringify: true,
				timestamp: true,
				level: process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'info'
			})
		]
	});
	logger.setLevels(logger.config.syslog.levels);

	var app = require('./app/app.js');
	app.run(8080);

} catch(e) {
	logger.error({
		message: e.message,
		details: e.stack
	});
}