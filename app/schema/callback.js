'use strict';
module.exports = new require('mongoose').Schema({
	type: String,
	method: String,
	url: String
}, {
	_id: false
});
