'use strict';
var mongoose = require('mongoose'),
	callbackSchema = require('./callback.js');

var jobSchema = new mongoose.Schema({
	active: Boolean,
	name: String,
	owner: String,
	rule: String,
	callback: callbackSchema
}, {
	timestamps: true
});

jobSchema.set('toJSON', {
	virtuals: true
});
jobSchema.options.toJSON.transform = function(doc, ret, options) {
	delete ret._id;
	delete ret.__v;
};

module.exports = jobSchema;