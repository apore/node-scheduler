'use strict';
module.exports = {
	run: run
};

// exports
var logger = require('winston'),
	status = require('http-status'),
	express = require('express'),
	bodyParser = require('body-parser'),
	request = require('request'),
	storage = require('./storage.js'),
    utils = require('util'),
	uuid = require('uuid/v4');


var server = null;
var app = null;
var router = null;

init();

function run(port) {
	server = app.listen(port);
}

function init() {
	app = express();
	router = express.Router();

	router.use(function(req, res, next) {
		var correlationId = req.get('X-Correlation-Id');
		if (!correlationId) {
			correlationId = uuid();
		}
		res.locals.correlationId = correlationId;
		res.locals.startTime = process.hrtime();
		res.locals.startTimestamp = (new Date()).toISOString();

		next();
	});

	router
		.get('/status', getStatusHandler)
		.post('/v1/jobs', createJobHandler)
		.get('/v1/jobs/:id', getJobByIdHandler)
		.patch('/v1/jobs/:id', updateJobPartiallyByIdHandler)
		.delete('/v1/jobs/:id', deleteJobByIdHandler)
		.post('/v1/jobs/:id/execute', executeJobHandler)
		.get('/v1/jobs', getJobsHandler);

	router.use(function(req, res, next) {
		var diff = process.hrtime(res.locals.startTime);
		logger.info({
			correlationId: res.locals.correlationId,
			startTime: res.locals.startTimestamp,
			label: 'trace',
			url: req.originalUrl,
			code: res.statusCode,
			method: req.method,
			duration: parseFloat(utils.format("%d", diff[0] + parseFloat(diff[1] / 1000000000)))
		});
		next();
	});

	app.set('json spaces', 2);
	app.use(bodyParser.urlencoded({limit: '1mb', extended: true}));
	app.use(bodyParser.json({limit: '1mb'}));
	app.use(router);

	initializeJobs();
}

function getStatusHandler(req, res, next) {
	res.status(status.OK).json({
		"status" : "OK",
		"numberOfScheduledJobs": numberOfRunningJobs,
		"memory": process.memoryUsage()
	});
	next();
}

/////////////////////
// JOBS

var schedule = require('node-schedule');
var lock = {};
var runningJobs = {};
var numberOfRunningJobs = 0;

function initializeJobs() {
	storage.getJobs().then(function(jobs) {
		logger.info('Found ' + jobs.length + ' jobs from databas!');
		for (var i = 0; i < jobs.length; i ++) {
			if (jobs[i].active) {
				scheduleJob(jobs[i]);
			}
		}
		logger.info('Executed ' + numberOfRunningJobs + '/' + jobs.length + ' jobs!');
	}, function(err) {
	});
}

function cancelJob(jobId) {
	if (typeof runningJobs[jobId] !== 'undefined') {
		numberOfRunningJobs --;
		runningJobs[jobId].cancel();
		delete runningJobs[jobId];
		logger.info(jobId + ' cancelled. Number of running jobs: ' + numberOfRunningJobs);
	}
}

function executeJob(job) {
	if (typeof lock[job.id] == 'undefined') {
		lock[job.id] = false;
	}

	if (lock[job.id]) {
		logger.debug(job.name + '/' + job.id + ': already running, skip!');
		return;
	}

	var startTime = process.hrtime();
	var startTimestamp = (new Date()).toISOString();

	lock[job.id] = true;

	// Do the job
	var cb = job.callback;

	request({
		url: cb.url,
		method: cb.method
	}, function(err, res, body) {
		lock[job.id] = false;
		logger.info(job.name + ' finished!');
		logger.debug(body);

		var diff = process.hrtime(startTime);
		logger.info({
			"startTime": startTimestamp,
			"label": "trace",
			"duration": parseFloat(utils.format("%d", diff[0] + parseFloat(diff[1] / 1000000000))),
			"jobName": job.name,
			"jobId": job.id
		});
	});
}

function scheduleJob(job) {
	logger.info(job.name + ' scheduled');
	numberOfRunningJobs ++;

	runningJobs[job.id] = schedule.scheduleJob(job.rule, executeJob.bind(null, job));
}

function createJobHandler(req, res, next) {
	storage.createJob(req.body).then(function(job) {
		logger.info('New job ' + job.name + ' has been created with id ' + job.id);
		res.status(status.CREATED).json(job);

		// If job is active then schedule it!!!!
		if (job.active) {
			scheduleJob(job);
		}
		next();
	}, function(err) {
		next(err);
	});
}

function getJobByIdHandler(req, res, next) {
	storage.getJobById(req.params.id).then(function(job) {
		res.status(status.OK).json(job);
		next();
	}, function(err) {
		res.status(status.NOT_FOUND);
		next(err);
	});
}

function updateJobPartiallyByIdHandler(req, res, next) {
	req.body.id = req.params.id;
	storage.updateJobPartially(req.body).then(function(job) {
		res.status(status.OK).json(job);
		if (job.active && typeof runningJobs[job.id] == 'undefined') {
			scheduleJob(job);
		} else if (!job.active) {
			cancelJob(job.id);
		}
	}, function(err) {
		next(err);
	});
}

function deleteJobByIdHandler(req, res, next) {
	cancelJob(req.params.id);
	storage.deleteJobById(req.params.id).then(function() {
		res.status(status.NO_CONTENT).json({});
		next();
	}, function(err) {
		res.status(status.NOT_FOUND);
		next(err);
	});
}

function getJobsHandler(req, res, next) {
	storage.getJobs().then(function(jobs) {
		res.status(status.OK).json(jobs);
		next();
	}, function(err) {
		next(err);
	});
}

function executeJobHandler(req, res, next) {
	res.status(status.OK).json({});

	storage.getJobById(req.params.id).then(function(job) {
		executeJob(job);
	}, function(err) {
		next(err);
	});
}

function shutdown() {
	logger.info("Received kill signal, shutting down gracefully.");

	// Cancel all jobs
	for (var i in runningJobs) {
		if (runningJobs.hasOwnProperty(i)) {
			cancelJob(i);
		}
	}

	server.close(function() {
		logger.info("Closed out remaining connections.");
		process.exit();
	});

	// if after
	setTimeout(function() {
		logger.info("Could not close connections in time, forcefully shutting down");
		process.exit();
	}, 8000);
}

// listen for TERM signal .e.g. kill
process.on ('SIGTERM', shutdown);

// listen for INT signal e.g. Ctrl-C
process.on ('SIGINT', shutdown);