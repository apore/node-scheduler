'use strict';
module.exports = {
	createJob: createJob,
	getJobs: getJobs,
	getJobById: getJobById,
	deleteJobById: deleteJobById,
	updateJobPartially: updateJobPartially
};

var mongoose = require('mongoose'),
	logger = require('winston'),
	jobSchema = require('./schema/job.js');

var Job = mongoose.model('job', jobSchema);

mongoose.Promise = Promise;

var userPass = '';
if (process.env.MONGO_USER && process.env.MONGO_PASS) {
	userPass = process.env.MONGO_USER + ':' + process.env.MONGO_PASS + '@'
}
var dbURI = 'mongodb://' + userPass + process.env.MONGO_URL + '/' + process.env.MONGO_DB;

var db = mongoose.connection;

db.on('disconnected', function() {
	logger.debug('disconnected, uri: %s', dbURI);
	connect();
});

connect();

function connect() {
	mongoose.connect(dbURI, {
		mongos: dbURI.indexOf(',') !== -1,
		server: {
			auto_reconnect: true,
			socketOptions: {
				keepAlive: 300000,
				connectionTimeoutMS: 1000
			}
		}
	});
	logger.info('MongoDB connection: ' + process.env.MONGO_URL + '/' + process.env.MONGO_DB);
}

function createJob(job) {
	return new Promise(function(resolve, reject) {
		Job.create(job, function(err, job) {
			if (err) {
				reject(err);
				return;
			}
			resolve(job);
		});
	});
}

function updateJobPartially(newJob) {
	return new Promise(function(resolve, reject) {
		Job.findById(newJob.id, function(err, job) {
			if (err) {
				reject(err);
				return;
			}

			if (newJob.active !== 'undefined') {
				job.active = newJob.active;
			}

			job.save(function(err, updatedJob) {
				if (err) {
					reject(err);
					return;
				}
				resolve(updatedJob);
			});
		});
	});
}

function getJobs() {
	return new Promise(function(resolve, reject) {
		Job.find().exec(function(err, jobs) {
			if (err) {
				reject(err);
				return;
			}
			resolve(jobs);
		});
	});
}

function getJobById(id) {
	return new Promise(function(resolve, reject) {
		Job.findById(id).exec(function(err, job) {
			if (err) {
				reject(err);
				return;
			}
			if (job === null) {
				reject(new Error("Not found"));
				return;
			}
			resolve(job);
		});
	});
}

function deleteJobById(id) {
	return new Promise(function(resolve, reject) {
		Job.findByIdAndRemove(id).exec(function(error, job) {
			if (error) {
				reject(error);
				return;
			}
			if (!job) {
				reject();
				return;
			}
			resolve(job);
		});
	});
}