#!/bin/sh

curl \
	-X POST \
	-H "Content-type: application/json;charset=utf-8" \
	--data @tests/request1.json \
	http://localhost:8080/v1/jobs

curl \
	-X POST \
	-H "Content-type: application/json;charset=utf-8" \
	--data @tests/request2.json \
	http://localhost:8080/v1/jobs

curl \
	-X POST \
	-H "Content-type: application/json;charset=utf-8" \
	--data @tests/request3.json \
	http://localhost:8080/v1/jobs

curl \
	-X POST \
	-H "Content-type: application/json;charset=utf-8" \
	--data @tests/request4.json \
	http://localhost:8080/v1/jobs

